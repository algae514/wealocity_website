package controllers;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

/**
 *
 * @author balu
 */
public class ReadWriteObjects {

//    private static final String GOOGLE_SERVER_KEY = "AIzaSyDA5dlLInMWVsJEUTIHV0u7maB82MCsZbU";
    private static final String GOOGLE_SERVER_KEY = "AIzaSyDdtgkKEJTkKdzgrFEJNnz-Ct9yhpJaCgM";
    static final String MESSAGE_KEY = "message";
    String fileLoc = "/home/ec2-user/wealocityserver/tokensList.txt";
    int counter = 0;

    public List<String> tokens = new ArrayList();

    private void saveObject() {

        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileLoc));
            objectOutputStream.writeObject(tokens);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                objectOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public void addToken(String token) {

        
        if(getAllTokensFromFile()!=null){

        tokens = getAllTokensFromFile();	
        }


        if (!tokens.contains(token)) {
            tokens.add(counter % 10, token);
            counter++;
            saveObject();
        }
    }

    public List<String> getAllTokensFromFile() {

        ObjectInputStream objectInputStream = null;
        try {


        	File yourFile = new File(fileLoc);
if(yourFile == null) {
    yourFile.createNewFile();
} 
FileInputStream oFile = new FileInputStream(yourFile); 


            objectInputStream = new ObjectInputStream(oFile);
            // start getting the objects out in the order in which they were written
            List<String> tokens = (List<String>) objectInputStream.readObject();
            System.out.println(" tokens "+tokens);

            return tokens;
        } catch (FileNotFoundException ex) {
            // Logger.getLogger(Rough.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
            System.out.println(" file does not exsits");
            return null;


        } catch (IOException ex) {
            // Logger.getLogger(Rough.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
                        return null;
                        
        } catch (ClassNotFoundException ex) {
            // Logger.getLogger(Rough.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
                        return null;
        } finally {
            try {
            	if(objectInputStream==null){
            		return null;
            	}
                objectInputStream.close();
            } catch (IOException ex) {
                // Logger.getLogger(Rough.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }

    }

    void tellEveryoneaMessage(String messageSt) {

        try {

if(tokens==null || tokens.size()==0){

	return;
}


            String userMessage = messageSt;
            Sender sender = new Sender(GOOGLE_SERVER_KEY);
            Message message = new Message.Builder().timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, userMessage).build();



            MulticastResult result = sender.send(message, tokens, 1);
            System.out.println("result = " + result);

//            request.setAttribute("pushStatus", result.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
//            request.setAttribute("pushStatus","RegId required: " + ioe.toString());
        } catch (Exception e) {
            e.printStackTrace();
//            request.setAttribute("pushStatus", e.toString());
        }

    }

}
