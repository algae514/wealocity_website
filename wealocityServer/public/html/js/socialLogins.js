/*
To be plced in header of html file



  <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="474642067891-dal0qdfm1raocqp6egrd1hdpfffpsllt.apps.googleusercontent.com">
    
    <script src="https://apis.google.com/js/platform.js" async defer></script>
     <script src="https://apis.google.com/js/api:client.js"></script>


*/



function onSignup() {

var loginConfPwd = $('#loginConfPwdSignup').val();
var loginPwd = $('#loginPwdSignup').val();
var loginEmail = $('#loginEmailSignup').val();

if(loginConfPwd == "" || loginPwd == "" || loginEmail == "" ){
    alert("All the fields are mandatory.");
    return;
}

if(loginConfPwd!=loginPwd){
    alert(" password and confirm password did not match");
    return;
}

/*
if(isEmail(loginEmail)){
    alert("Invalid email, please enter a valid email.");
    return;
}
*/
// alert(" in signup: loginConfPwd > "+loginConfPwd+" loginPwd> "+loginPwd+" loginEmail>"+loginEmail);
submitToServer(loginEmail,loginPwd);

hideModals();

}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}





 // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();


    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '193027517713878',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + JSON.stringify(response));

      var name = response.name;

      var email = response.email;
      if(email==null || email == "" || email==undefined){
        email="No_Emailprovidedfor_FB";
      }
      
      subitEmailToServerFrom(name,email);

    });
  }





  function loginToFB(){

FB.init({
    appId      : '193027517713878',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });


  }










var googleUser = {};
  var startApp = function() {
    console.log(" startApp ");


    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '474642067891-dal0qdfm1raocqp6egrd1hdpfffpsllt.apps.googleusercontent.com',
        cookiepolicy: 'none',
        // Request scopes in addition to 'profile' and 'email'
        scope: 'profile email'
      });

      var btn = document.getElementById('customGooglePBtn');
      console.log(" btn for google plus "+btn);
      attachSignin(auth2,btn);
    });


  };

  function attachSignin(auth2,element) {
    console.log("attachSignin : "+element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
  var name = googleUser.getBasicProfile().getName();
  var email = googleUser.getBasicProfile().getEmail();
    subitEmailToServerFrom(name,email)
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }



