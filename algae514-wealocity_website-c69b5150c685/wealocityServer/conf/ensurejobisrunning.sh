#!/bin/bash
service=wealocity

echo $(ps -ef | grep -v grep | grep -i "wealocity" | wc -l)

if (( $(ps -ef | grep -v grep | grep -i "wealocity" | wc -l) > 2 ))
then
echo "$service is running!!!"
else
rm -rf /home/ec2-user/wealocityserver/RUNNING_PID

/home/ec2-user/wealocityserver/bin/wealocityserver -Dhttp.port=80 &

fi

